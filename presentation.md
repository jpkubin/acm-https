---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url("https://marp.app/assets/hero-background.svg")
---

# **HTTP Requests & APIs**
### Web Development Part 3
### W&M ACM

#### 03/23/2023

---

# **Road Map**

1. ACM / Cypher Updates
1. HTTP/API Workshop (~6:35)
2. Quiplash! (~7:00)
3. Technical Interview Prep Info Session (~7:20)
4. 2nd-to-last Cypher Meeting (~7:30)

---

# **Cypher Updates!**

#### MARCH 31-APRIL 2ND **(that's less than two weeks from now)**

* We need humans!
    * Day-of student volunteers
    * Adult mentors and judges
    * ✨PARTICIPANTS✨

Stay after today for a planning meeting!

---

# **Today's Project**

We will be building our own simple API in Flask (a Python module!) using an existing API.

Things you need:
* Python/PIP installed (An Anaconda installation from 141 works great!)
* An IDE
* Flask `pip install --upgrade flask`
* Requests `pip install --upgrade requests`

---

![bg right](https://c8.alamy.com/comp/E7WM6D/close-up-of-computer-screen-internet-browser-address-bar-with-blank-E7WM6D.jpg)

# **What is HTTP?**

* The beginning of every URL, of course!
* **Hypertext Transfer Protocol** (HTML is the **Hypertext Markup Language**)
    * Hypertext = Text + Links
* Deals with *requests* and *responses* to a **web services**

---

# **API**

* Stands for **Application Programming Interface**
* Used for the **Front End** of an application (HTML page, mobile app) to connect to the **Back End** (the program running on a web server, the database, etc)
* Used for other applications to access a data (many of your favorite apps have APIs)
    * e.g. Spotify Iceberg uses the Spotify API to get data about a user's listening history and modify it himself

---

# **Getting Started**

---

# **Anatomy of a HTTP Request**

* **Method (a.k.a. HTTP verb)**
    * Type of request being made (the precise implementation details are up to the server)
    * Most prominent are **GET** (think URL bar) and **POST** (form submission)
* **Headers**
    * Key-value pairs providing details about the request (authentication, origin site)
* **Body** (the data we are sending)