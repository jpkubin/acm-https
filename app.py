import requests
from flask import Flask, render_template, request
from datetime import date
import random

RESPONSES = {
    "A": ('Python', "You're dynamic, interpreted, and great for learning and prototyping!"),
    "B": ('Java', "You're strictly object oriented but loved by generations for your smart runtine!"),
    "C": ('C', "You're compiled, procedural, and focused on the implementation details!")
}

app = Flask(__name__)

@app.route('/', methods=['GET'])
def home():
    today = str(date.today())
    return render_template('index.html', today=today)

@app.route('/quiz', methods=['GET'])
def quiz():
    return render_template('quiz.html')

@app.route('/pokemon', methods=['GET'])
def pokemon():
    num = random.randint(1, 1010)
    data = requests.get(f'https://pokeapi.co/api/v2/pokemon/{num}').json()
    image = data['sprites']['other']['official-artwork']['front_default']
    name = data['species']['name']
    name = name[0].upper() + name[1:]
    return render_template('pokemon.html', image=image, pokemon=name)

@app.route('/quiz/results', methods=["POST", "GET"])
def results():
    if request.method == 'GET' or not request.form:
        # no results passed
        return render_template('results.html')
    results = {x: 0 for x in request.form.values()}
    for key in request.form.keys():
        results[request.form[key]] += 1
    final = sorted(results.keys(), key=lambda x: results[x])[0]
    result, desc = RESPONSES[final]
    return render_template('results.html', result=result, desc=desc)
    